#include "treev.h"

using namespace std;

void treapv::update_sum(int idx) {
    if(idx != -1) {
        int left_sum = t[idx].leftNodeIdx != -1 ? t[t[idx].leftNodeIdx].sum : 0;
        int right_sum = t[idx].rightNodeIdx != -1 ? t[t[idx].rightNodeIdx].sum : 0;

        t[idx].sum = left_sum + right_sum + t[idx].key;
    }
}


long long treapv::sum_private(int l, int r, int curNodeIdx) const {
    if (curNodeIdx == -1) {
        return 0;
    }
    if (l > r) {
        return 0;
    }

    if (t[curNodeIdx].key <= l) {
        return sum_private(l, r, t[curNodeIdx].rightNodeIdx);
    }
    if (t[curNodeIdx].key > r) {
        return sum_private(l, r, t[curNodeIdx].leftNodeIdx);
    }

    return t[curNodeIdx].key +
           sum_private(l, r, t[curNodeIdx].rightNodeIdx) +
           sum_private(l, r, t[curNodeIdx].leftNodeIdx);
}


void treapv::add(int key) {
    int value = ++valueGenerator;
    ++size;

    if(root == -1) {
        Node newNode(key, value, -1, -1);
        newNode.sum = newNode.key;
        t.push_back(newNode);
        root = 0;
        return;
    }

    int nodeIdx = root;
    int parentIdx = -1;

    while(nodeIdx != -1) {
        parentIdx = nodeIdx;
        if(key < t[nodeIdx].key) {
            // РІРѕС‚ С‚СѓС‚ С‚РµСЂСЏРµС‚СЃСЏ СЃРІСЏР·СЊ???
            nodeIdx = t[nodeIdx].leftNodeIdx;
        } else {
            nodeIdx = t[nodeIdx].rightNodeIdx;
        }
    }

    int leftTreeIdx = -1;
    int rightTreeIdx = -1;
    split(nodeIdx, key, leftTreeIdx, rightTreeIdx);

    Node newNode(key, value, -1, -1);
    newNode.sum = newNode.key;
    t.push_back(newNode);
    int newNodeIdx = t.size() - 1;

    if(parentIdx == -1) {
        root = newNodeIdx;
        update_sum(root);
    } else if(key < t[parentIdx].key) {
        t[parentIdx].leftNodeIdx = newNodeIdx;
        update_sum(parentIdx);
    } else {
        t[parentIdx].rightNodeIdx = newNodeIdx;
        update_sum(parentIdx);
    }

    t[newNodeIdx].leftNodeIdx = leftTreeIdx;
    t[newNodeIdx].rightNodeIdx = rightTreeIdx;

    update_sum(newNodeIdx);
    update_sum(parentIdx);
}


void treapv::merge(int &currNode, int &left, int &right) {
    if(left == -1 || right == -1) {
        currNode = left != -1 ? left : right;
    } else if(t[left].key > t[right].key) {
        merge(t[left].rightNodeIdx, t[left].rightNodeIdx, right);
        currNode = left;
    } else {
        merge(t[right].leftNodeIdx, left, t[right].leftNodeIdx);
        currNode = right;
    }
    update_sum(currNode);
}


void treapv::delete_private(int &currNode, int value) {
    if(currNode == -1) return;

    if(t[currNode].key == value) {
        --size;
        merge(currNode, t[currNode].leftNodeIdx, t[currNode].rightNodeIdx);
    } else {
        delete_private(t[currNode].leftNodeIdx, value);
        delete_private(t[currNode].rightNodeIdx, value);
    }
}


void treapv::split(int &currNodeIdx, int key, int &leftIdx, int &rightIdx) {
    if(currNodeIdx == -1) {
        leftIdx = rightIdx = -1;
    } else if(t[currNodeIdx].key <= key) {
        split(t[currNodeIdx].rightNodeIdx, key, t[currNodeIdx].rightNodeIdx, rightIdx);
        leftIdx = currNodeIdx;
    } else {
        split(t[currNodeIdx].leftNodeIdx, key, leftIdx, t[currNodeIdx].leftNodeIdx);
        rightIdx = currNodeIdx;
    }
    update_sum(currNodeIdx);
}


treapv::treapv(const treapv &treap_from) {
    root = treap_from.root;
    size = treap_from.size;
    t = treap_from.t;
    valueGenerator = treap_from.valueGenerator;
}


treapv &treapv::operator=(const treapv &treap_from) {
    if(&treap_from != this) {
        root = treap_from.root;
        size = treap_from.size;
        t = treap_from.t;
        valueGenerator = treap_from.valueGenerator;
        return *this;
    }
}

