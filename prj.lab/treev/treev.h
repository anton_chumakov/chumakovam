#ifndef VECTOR_H
#define VECTOR_H



#include <vector>
#include <iostream>

class treapv {
private:
    struct Node {
        int key = 0;
        int value = 0;
        int sum = 0;

        Node () = default;
        ~Node() = default;

        int rightNodeIdx = -1;
        int leftNodeIdx = -1;

        explicit Node(int k): key(k), value(0), sum(k)
        {}

        Node (int k, int v, int leftNodeIdx, int rightNodeIdx):
                key(k), value(v), leftNodeIdx(leftNodeIdx), rightNodeIdx(rightNodeIdx)
        {}
    };

public:
    treapv() = default;
    ~treapv() = default;
    treapv(const treapv &treap_from);
    treapv& operator =(const treapv& treap_from);

    void add(int key);


    void deleteElement(const int value) {
        delete_private(root, value);
    }



    long long sum(const int l, const int r) const {
        return sum_private(l, r, root);
    };



    int getSize() const {
        return size;
    }

    void clear() {
        t.clear();
        size = 0;
        valueGenerator = 0;
        root = -1;
    }

private:
    void split(int &currNodeIdx, int key, int &leftIdx, int &rightIdx);
    void merge(int &currNode, int &left, int &right);
    void update_sum(int idx);

    void delete_private(int &currNode, int value);
    long long sum_private(int l, int r, int curNodeIdx) const;

    int size = 0;
    int root = -1;
    int valueGenerator = 0;
    std::vector<Node> t;
};

#endif //VECTOR_H
