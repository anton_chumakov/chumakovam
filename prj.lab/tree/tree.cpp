#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <time.h> 
#include "Tree.h"
#include <random>
#include <chrono>
#include <ctime>   
#include <thread>

using namespace templates;
using namespace std; 
using namespace std::this_thread;
using namespace std::chrono;

double sum_time = 0;

int test()
{
	cTree tre;

	srand(time(0));
	int random_size = rand() % 20 + 1;

	int i = 0;
	int* ar = new int[random_size];
	int rand_el = 0;
	double avertime = 0;


	for (i = 0; i < random_size; i++)
	{
		rand_el = rand() + 1;
		tre.AddToTree(rand_el);
		ar[i] = rand_el;
	}

	int k = 0;
	int sum_real = 0;
	int sum_count = 0;
	int check_el = 0;
	int error = 0;

	for (i = 0; i < random_size; i++)
	{
		check_el = ar[i];
		std::cout << ar[i] << ' ';
		sum_real = 0;
		for (k = 0; k < random_size; k++)
		{
			if (ar[k] < check_el)
			{
				sum_real = sum_real + ar[k];
			}
		}
		std::cout << sum_real << ' ';
		auto start = std::chrono::system_clock::now();
		sum_count = tre.sum_less(check_el);
		auto end = std::chrono::system_clock::now();
		std::chrono::duration<double> elapsed_seconds = (end - start);
		double res = elapsed_seconds.count();
		avertime = avertime + res;
		std::cout << sum_count << '\n';
		if (sum_real != sum_count)
		{
			error = error + 1;
		}
	}

	avertime = avertime / (random_size);

	std::cout << "Sum_less duration " << avertime << "s\n";

	sum_time = sum_time + avertime;

	if (error == 0)
	{
		std::cout << "It works!\n";
	}
	else
	{
		std::cout << "It doesn't work!\n";
	}

	tre.PrintSorted(); //Вывод от самого маленького к самому большому

	for (i = 0; i < random_size; i++)
	{
		check_el = ar[i];
		//Номер "строки"(сверху) и "столбца"(слева) элемента - как их нужно было бы рисовать
		std::cout << tre.Numbers(check_el).value << ' ' << tre.Numbers(check_el).stroka << ' ' << tre.Numbers(check_el).stolbec << '\n';
	}

	delete[] ar;
	tre.DeleteTree();

	return error;
}

int main()
{
	srand(time(0));
	int numberoftests = rand() % 10 + 1;
	int i = 0;
	int sum_error = 0;
	for (i = 0; i < numberoftests; i++)
	{
		sum_error = sum_error + test();
		sleep_for(seconds(1)); //чтобы srand(time(0)) успел обновиться
	}

	sum_time = sum_time / numberoftests;
	std::cout << "\n SUM_LESS AVERAGE DURATION " << sum_time << "s\n";

	if (sum_error == 0)
	{
		std::cout << "\n ALL RANDOM TESTS WORK!\n";
	}
	else
	{
		std::cout << "It doesn't work!\n";
	}
}
