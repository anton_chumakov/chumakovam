#ifndef TEMPLATES_TREE
#define TEMPLATES_TREE

namespace templates
{
    class cTree
    {
    private:
        struct treeElement
        {
            int value;
            treeElement* left;
            treeElement* right;
            treeElement(int _data, treeElement* _left, treeElement* _right)
            {
                value = _data;
                left = _left;
                right = _right;
            }
        };

    public:
        struct Number
        {
            int value;
            int stroka;
            int stolbec;
            Number(int _value, int _stroka, int _stolbec)
            {
                value = _value;
                stroka = _stroka;
                stolbec = _stolbec;
            }
        };
    public:

        cTree()
        {
            root = 0;
        }

        virtual ~cTree()
        {
        }

        void DeleteTreeRec(treeElement* e)
        {
            if (!e)
                return;
            DeleteTreeRec(e->left);
            DeleteTreeRec(e->right);
            delete e;
        }

        void DeleteTree()
        {
            if (root == NULL)
                return;
            DeleteTreeRec(root);
        }

        void AddToTree(int value)
        {
            treeElement* prev = NULL;
            treeElement* current = root;

            while (current)
            {
                prev = current;
                if (value > current->value)
                    current = current->right;
                else if (value < current->value)
                    current = current->left;
                else
                    return;
            }

            treeElement* new_element = new treeElement(value, NULL, NULL);

            if (!prev)
            {
                root = new_element;
            }
            else
            {
                if (value > prev->value)
                    prev->right = new_element;
                else
                    prev->left = new_element;
            }
        }

        int IsInTree(int value)
        {
            treeElement* current = root;

            while (current)
            {
                if (value > current->value)
                    current = current->right;
                else if (value < current->value)
                    current = current->left;
                else
                    return 1;
            }

            return 0;

        }

        void PrintSortedRec(treeElement* e)
        {
            if (!e)
                return;

            PrintSortedRec(e->left);
            std::cout << e->value << '\n';
            PrintSortedRec(e->right);
        }

        void PrintSorted() //����� �� ������ ���������� � ������ ��������
        {
            if (!root)
                std::cout << ("Tree is empty");

            PrintSortedRec(root);
            std::cout << ('\n');
        }

        Number Numbers(int n) //����� "������"(������) � "�������"(�����) ��������
        {
            Number nk(0, 0, 0);
            if ((IsInTree(n)) == 0)
            {
                nk.value = n;
                nk.stroka = 0;
                nk.stolbec = 0;
                return (nk);
            }
            else
            {
                int i = 1;
                int j = 1;
                treeElement* e = root;
                if (n == e->value)
                {
                    nk.value = n;
                    nk.stroka = i;
                    nk.stolbec = j;
                }
                else
                {
                    while (e->value != n)
                    {
                        if (n > e->value)
                        {
                            e = e->right;
                            j = j * 2;
                        }
                        else
                        {
                            e = e->left;
                            j = j * 2 - 1;
                        }

                        ++i;
                    }
                    nk.value = n;
                    nk.stroka = i;
                    nk.stolbec = j;
                }
                return(nk);
            }
        }

        int summ(treeElement* current) //����� 
        {
            int sum = current->value;
            int sum_left = 0;
            int sum_right = 0;
            if (current->left)
                sum_left = summ(current->left);
            if (current->right)
                sum_right = summ(current->right);
            sum = sum+sum_left + sum_right;
            return sum;
        }

        int sum_less(int element) //����� ���� ���������, �������, ��� element
        {
            if ((IsInTree(element)) == 0)
                return -1;
            else 
            {
                treeElement* current = root;
                int sum = 0;
                while (current)
                {
                    if (element > current->value)
                    {
                        sum = sum + current->value;
                        if (current->left)
                            sum = sum + summ(current->left);
                        current = current->right;
                    }
                    else if (element < current->value)
                        current = current->left;
                    else
                        if (current->left == NULL)
                            return sum;
                        else
                            return (summ(current->left)+sum);
                }
            }
            
        }

    private:
        treeElement* root;
    };

};




#endif //#ifndef TEMPLATES_TREE#pragma once
#pragma once