#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include "queue.h"
#include <chrono>
#include <ctime>    


using namespace std;


int main()
{
    CQueue que;
    float s = 5112221.012f;
    float d = 155336.223333f;
    float ch;

    auto start = std::chrono::system_clock::now();
    for (int i = 0; i < 100; ++i)
    {
        que.push(s);
    }
    auto end = std::chrono::system_clock::now();


    auto start_1 = std::chrono::system_clock::now();
    for (int i = 0; i < 100; ++i)
    {
        que.pop();
    }
    auto end_1 = std::chrono::system_clock::now();

    std::chrono::duration<double> elapsed_seconds = (end - start) / 100;
    std::chrono::duration<double> elapsed_seconds_1 = (end_1 - start_1) / 100;

    std::cout << "Push duration " << elapsed_seconds.count() << "s\n";
    std::cout << "Pop duration " << elapsed_seconds_1.count() << "s\n";
  




}
