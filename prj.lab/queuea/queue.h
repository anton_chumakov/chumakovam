#ifndef TEMPLATES_LIST_2020_02_24
#define TEMPLATES_LIST_2020_02_24

class CQueue
	{
	public:
		CQueue() = default;

		void push(float data_push) 
		{
			if (buffersize == 0)
			{
				buffersize = 8;
				data_ = new float[buffersize];
				head = 0;
			}

			if ((++tail % buffersize) == (head))
			{
				buffersize = buffersize * 2;
				float* new_buffer = new float[buffersize];
				if (tail >= head)
				{ 
					for (int i = head; i < tail + 1; ++i)
						new_buffer[i-head] = data_[i];
				}
				else
				{
					for (int i = head; i < (buffersize / 2) + 1; ++i)
					{
						new_buffer[i] = data_[i];
					}
						
					for (int i = 0; i < tail; ++i)
					{
						new_buffer[buffersize / 2 - head + i] = data_[i];
					}
				}
				std::swap(data_, new_buffer);
				delete[] new_buffer;
			}

			tail = (tail + 1) % buffersize;
			data_[tail] = data_push;

		}
		 
		bool is_empty() const
		{
			if (head == -1) return 1;
			return 0;
		}

		void pop()
		{
			if (is_empty()) {
				throw std::logic_error("QueueA::dequeue() - empty queue");
			}
			else {
				head = (head + 1) % buffersize;
				if (head == tail) {
					tail = 0;
					head = -1;
				}
			}
		}

		float &top()
		{
			if (is_empty()) {
				throw std::logic_error("QueueA::top() - empty queue");
			}
			return data_[head];
		}
/*
		int getSize()
		{
			return(finish - start);
		}

		void clear()
		{
			finish = 0;
			start = 0;
		}*/

		//void print()
		//{
		//	for (int i = head; i < tail+1 ; ++i)
		//	{
		//		std::cout << data_[i];
		//	}
		//}
		
	private:
		float* data_{ nullptr };
		int head{ -1 };
		int tail{ -1 };
		int buffersize{ 0 };

	};

#endif //#ifndef TEMPLATES_LIST_2020_02_24#pragma once
#pragma once
