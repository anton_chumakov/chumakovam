#ifndef TEMPLATES_LIST_2020_01_01
#define TEMPLATES_LIST_2020_01_01

namespace templates
{
    template<class T>
    class CQueue
    {
    private:
        struct leaf
        {
            T data;
            leaf* pnext;
            leaf(T& _data, leaf* _pnext = NULL)
            {
                data = _data;
                pnext = _pnext;
            }
        };
    public:

        CQueue()
        {
            m_pBegin = 0;
            m_pEnd = 0;
        }

        void InQueue(T& data)
        {
            leaf* l = new leaf(data, NULL);
            if (m_pBegin == NULL)
            {
                m_pBegin = l;
            }
            else
            {
                m_pEnd->pnext = l;
            }
            m_pEnd = l;
        }


        T DeQueue()
        {
            leaf* l = m_pBegin;
            if (l == NULL)
            {
                throw new std::exception();
            }

            m_pBegin = m_pBegin->pnext;
            T c = l->data;
            delete l;
            return c;
        }

        bool isQueueEmpty()
        {
            if (m_pBegin) return false;
            else return true;
        }

        int getSize()
        {
            leaf* l = m_pBegin;
            int size = 0;
            while (l)
            {
                ++size;
                l = l->pnext;
            }
            return size;
        }

        void clear()
        {
            leaf* l = m_pBegin;
            while (l)
            {
                leaf* temp = l->pnext;
                delete(l);
                l = temp;
            }
        }

        void print()
        {
            leaf* l = m_pBegin;
            while (l)
            {
                std::cout << l->data << '\n';
                l = l->pnext;
            }
        }

    private:
        leaf* m_pBegin, * m_pEnd;
    };
};
#endif //#ifndef TEMPLATES_LIST_2020_01_01#pragma once
#pragma once
