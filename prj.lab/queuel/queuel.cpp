#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include "queue.h"
#include <chrono>
#include <ctime>    


using namespace templates;
using namespace std;



int main()
{
    CQueue <std::string> que;
    std::string s = "Hello";
    std::string d = "World";
    std::string ch;

    //test(que.isQueueEmpty(), 1);

    auto start = std::chrono::system_clock::now();
    for(int i = 0; i < 1000; ++i)
    {
        que.InQueue(s);
    }
    auto end = std::chrono::system_clock::now();

    //test(que.getSize(), 100000);
    //test(que.isQueueEmpty(), 0);

    //que.InQueue(s);
    //test(que.DeQueue(), s);
    
    auto start_2 = std::chrono::system_clock::now();
    que.print();
    auto end_2 = std::chrono::system_clock::now();
    
    for (int k = 0; k < 100; ++k)
    {

    }

    auto start_1 = std::chrono::system_clock::now();
    for (int i = 0; i < 1000; ++i)
    {
        ch = que.DeQueue();
    }
    auto end_1 = std::chrono::system_clock::now();

    std::chrono::duration<double> elapsed_seconds = (end - start) / 1000;
    std::chrono::duration<double> elapsed_seconds_1 = (end_1 - start_1) / 1000;
    std::chrono::duration<double> elapsed_seconds_2 = end_2 - start_2;
    std::chrono::duration<double> elapsed_seconds_2_1 = (end_2 - start_2) / 1000;

    std::cout <<  "InQueue duration " << elapsed_seconds.count() << "s\n";
    std::cout << "DeQueue duration " << elapsed_seconds_1.count() << "s\n";
    std::cout << "PrintQueue duration " << elapsed_seconds_2.count() << "= 100000 * " << elapsed_seconds_2_1.count() << "s\n";

    que.clear();
}

